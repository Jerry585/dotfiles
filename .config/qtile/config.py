import os
import subprocess
from libqtile import bar, layout, qtile, hook
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from qtile_extras import widget
from qtile_extras.widget.decorations import PowerLineDecoration

mod = "mod4"
terminal = "alacritty"

keys = [
    # Switch between windows
    Key([mod], "h", 
        lazy.layout.left(), 
        desc="Move focus to left"
    ),
    Key(
        [mod], "l", 
        lazy.layout.right(), 
        desc="Move focus to right"
    ),
    Key([mod], "j", 
        lazy.layout.down(), 
        desc="Move focus down"
    ),
    Key([mod], "k", 
        lazy.layout.up(), 
        desc="Move focus up"
    ),
    Key([mod], "space", 
        lazy.layout.next(), 
        desc="Move window focus to other window"
    ),
    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", 
        lazy.layout.shuffle_left(), 
        desc="Move window to the left"
    ),
    Key([mod, "shift"], "l", 
        lazy.layout.shuffle_right(), 
        desc="Move window to the right"
    ),
    Key([mod, "shift"], "j", 
        lazy.layout.shuffle_down(), 
        desc="Move window down"
    ),
    Key([mod, "shift"], "k", 
        lazy.layout.shuffle_up(), 
        desc="Move window up"
    ),
    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", 
        lazy.layout.grow_left(), 
        desc="Grow window to the left"
    ),
    Key([mod, "control"], "l", 
        lazy.layout.grow_right(), 
        desc="Grow window to the right"
    ),
    Key([mod, "control"], "j", 
        lazy.layout.grow_down(), 
        desc="Grow window down"
    ),
    Key([mod, "control"], "k", 
        lazy.layout.grow_up(), 
        desc="Grow window up"
    ),
    Key([mod], "n", 
        lazy.layout.normalize(), 
        desc="Reset all window sizes"
    ),
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key(
        [mod, "shift"],
        "Return",
        lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack",
    ),
    Key([mod], "Return", 
        lazy.spawn(terminal), 
        desc="Launch terminal"
    ),
    # Toggle between different layouts as defined below
    Key([mod], "Tab", 
        lazy.next_layout(), 
        desc="Toggle between layouts"
    ),
    Key([mod], "w", 
        lazy.window.kill(), 
        desc="Kill focused window"
    ),
    Key(
        [mod],
        "f",
        lazy.window.toggle_fullscreen(),
        desc="Toggle fullscreen on the focused window",
    ),
    Key([mod], "t", 
        lazy.window.toggle_floating(), 
        desc="Toggle floating on the focused window"
    ),
    Key([mod, "control"], "r", 
        lazy.reload_config(), 
        desc="Reload the config"
    ),
    Key([mod, "control"], "q", 
        lazy.shutdown(), 
        desc="Shutdown Qtile"
    ),
    Key([mod], "r", 
        lazy.spawn("rofi -show drun"), 
        desc="Spawn a command using a prompt widget"
    ),
]

# Add key bindings to switch VTs in Wayland.
# We can't check qtile.core.name in default config as it is loaded before qtile is started
# We therefore defer the check until the key binding is run by using .when(func=...)
for vt in range(1, 8):
    keys.append(
        Key(
            ["control", "mod1"],
            f"f{vt}",
            lazy.core.change_vt(vt).when(func=lambda: qtile.core.name == "wayland"),
            desc=f"Switch to VT{vt}",
        )
    )


groups = [Group(i) for i in "123456789"]

for i in groups:
    keys.extend(
        [
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name, switch_group=True),
                desc="Switch to & move focused window to group {}".format(i.name),
            ),
        ]
    )

colors  = [
    ["#282828", "#282828"], # bg
    ["#ebdbb2", "#ebdbb2"], # fg
    ["#cc241d", "#cc241d"], # color 01
    ["#98971a", "#98971a"], # color 02
    ["#d79921", "#d79921"], # color 03
    ["#458588", "#458588"], # color 04
    ["#b16286", "#b16286"], # color 05
    ["#689d6a", "#689d6a"], # color 06
    ["#a89984", "#a8998a"], # color 07
    ["#928374", "#928374"], # color 08
    ["#fb4934", "#fb4934"], # color 09
    ["#b8bb26", "#b8bb26"], # color 10
    ["#fabd2f", "#fabd2f"], # color 11
    ["#83a598", "#83a598"], # color 12
    ["#d3869b", "#d3869b"], # color 13
    ["#8ec07c", "#8ec07c"], # color 14
]

powerlineLeft = {
    "decorations": [
        PowerLineDecoration(
            path = "arrow_left",
        ),
    ]
}

powerlineRight = {
    "decorations": [
        PowerLineDecoration(
            path = "arrow_right",
        ),
    ]
}

layouts = [
    layout.MonadTall(
        border_width = 2,
        margin = 8,
        border_focus = colors[6],
        border_normal = colors[0]
    ),
    layout.Max(),
]

widget_defaults = dict(
    font="sans",
    fontsize=14,
    padding=3,
    background=colors[0],
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.CurrentLayout(
                    foreground = colors[0],
                    background = colors[13],
                    padding = 5,
                    **powerlineLeft,
                ),
                widget.GroupBox(
                    background = colors[5],
                    fontsize = 12,
                    margin_y = 3,
                    margin_x = 5,
                    borderwidth = 3,
                    active = colors[1],
                    inactive = colors[1],
                    rounded = True,
                    highlight_color = colors[0],
                    highlight_method = "block",
                    this_current_screen_border = colors[0],
                    this_screen_border = colors[13],
                    other_current_screen_border = colors[0],
                    other_screen_border = colors[13],
                    **powerlineLeft,
                ),
                widget.Prompt(
                    foreground = colors[1],
                ),
                widget.Spacer(),
                widget.WindowName(
                    foreground = colors[1],
                    max_chars = 40,
                    padding = 5,
                    width = bar.CALCULATED,
                    **powerlineRight,
                ),
                widget.Spacer(
                    **powerlineRight,
                ),
                widget.Systray(
                    foreground = colors[1],
                    background = colors[5],
                ),
                widget.Volume(
                    foreground = colors[1],
                    background = colors[5],
                ),
                widget.CPU(
                    foreground = colors[1],
                    background = colors[5],
                    format = 'CPU {load_percent}%'
                ),
                widget.Memory(
                    foreground = colors[1],
                    background = colors[5],
                    **powerlineRight,
                ),
                widget.Clock(
                    format = "%Y-%m-%d %a %I:%M %p",
                    foreground = colors[0],
                    background = colors[13],
                ),
                widget.QuickExit(
                    foreground = colors[0],
                    background = colors[13],
                    default_text = '[X]',
                ),
            ],
            24,
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
floats_kept_above = True
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
