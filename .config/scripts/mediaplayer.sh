#!/bin/bash

while true
do 
    playerStatus=$(playerctl -p spotify status 2> /dev/null)

    if [ "$playerStatus" = "Playing" ]; then
        echo '{"text": "'" $(playerctl -p spotify metadata artist) - $(playerctl -p spotify metadata title)"'", "class": "custom-spotify"}'
    elif [ "$playerStatus" = "Paused" ]; then
        echo '{"text": "'" $playerStatus"'", "class": "custom-spotify"}'
    else
        echo '{"text": "", "class": "custom-spotify"}'
    fi
done
